from django.db import models


class CreationDateMixin(models.Model):
    """
    Abstract base class with a creation date and time
    """

    class Meta:
        abstract = True
        get_latest_by = "created"

    created = models.DateTimeField(
        verbose_name="creation date and time", auto_now_add=True
    )


class ModificationDateMixin(models.Model):
    """
    Abstract base class with a modification date and time
    """

    class Meta:
        abstract = True
        get_latest_by = "last_modified"

    last_modified = models.DateTimeField(
        verbose_name="modification date and time", auto_now=True
    )
