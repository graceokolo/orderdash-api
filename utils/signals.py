from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete

from general.models import CustomerPhoneNumber, CustomerAddress
from order.models import OrderProduct
from product.models import Product, ProductVersion
from shared.parameters import DEPARTMENT_BAR
from user.models import Profile, Department


@receiver(post_save, sender=OrderProduct)
def change_products_details(instance, created, **kwargs):
    if created:
        product = instance.product.product
        ordered_quantity = instance.ordered_quantity
        quantity_in_stock = product.quantity_in_stock
        Product.objects.filter(pk=product.id).update(quantity_in_stock=(quantity_in_stock - ordered_quantity))


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        bar_department_instance, _ = Department.objects.get_or_create(name=DEPARTMENT_BAR)
        Profile.objects.create(user=instance, department=bar_department_instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


@receiver(post_delete, sender=CustomerPhoneNumber)
def delete_customer_phone_number(instance, **kwargs):
    instance.phone_number.delete()


@receiver(post_save, sender=Product)
def save_product(sender, instance, created, **kwargs):
    product_version, _ = ProductVersion.objects.get_or_create(
        unit_price=instance.unit_price,
        product=instance,
        is_active=True
    )
    all_product_versions = ProductVersion.objects\
        .exclude(id=product_version.id).filter(product=instance).update(is_active=False)
    all_product_versions


@receiver(post_delete, sender=CustomerAddress)
def delete_customer_address(instance, **kwargs):
    instance.address.delete()

# @receiver(post_save, sender=Order)
# def change_table_status_occupied(instance, created, **kwargs):
#     if created:
#         table = instance.table
#         occupied_status = TableStatus.objects.get(value=TABLE_STATUS_OCCUPIED)
#         Table.objects.filter(pk=table.id).update(status=occupied_status)
#
#
# @receiver(post_delete, sender=Order)
# def change_table_status_vacant(instance, **kwargs):
#     table = instance.table
#     vacant_status = TableStatus.objects.get(value=TABLE_STATUS_VACANT)
#     Table.objects.filter(pk=table.id).update(status=vacant_status)
