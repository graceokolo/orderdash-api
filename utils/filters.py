from datetime import datetime
from django.utils import timezone
from django.utils.timezone import get_current_timezone
from rest_framework import filters


class SalesDurationFilterBackend(filters.BaseFilterBackend):
    """
    Filter shows order for a time duration.
    """
    def filter_queryset(self, request, queryset, view):
        today_date = timezone.now()
        tz = get_current_timezone()
        start_date = request.query_params.get('start_date', None)
        end_date = request.query_params.get('end_date', None)
        if start_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d')
            start_date = start_date.replace(tzinfo=tz)
            if end_date:
                end_date = datetime.strptime(end_date, '%Y-%m-%d')
                end_date = end_date.replace(tzinfo=tz)
            else:
                end_date = today_date
            return queryset.filter(created__date__lte=end_date).filter(created__gte=start_date)
        else:
            return queryset.filter()
