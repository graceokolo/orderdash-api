from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from customer.models import Customer
from order.models import Order
from product.models import Product
from summary.serializers import CustomerOrderSummarySerializer, OrderSummarySerializer, ProductSalesSummarySerializer
from utils.filters import SalesDurationFilterBackend


class CustomerOrderSummaryViewSet(
    RetrieveModelMixin,
    GenericViewSet,
):
    serializer_class = CustomerOrderSummarySerializer
    permission_classes = [IsAuthenticated]
    queryset = Customer.objects.all()


class OrderSummaryViewSet(
    ListModelMixin,
    GenericViewSet,
):
    serializer_class = OrderSummarySerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [SalesDurationFilterBackend]
    queryset = Order.objects.all()


class ProductSalesSummaryViewSet(
    ListModelMixin,
    GenericViewSet,
):
    serializer_class = ProductSalesSummarySerializer
    permission_classes = [IsAuthenticated]
    queryset = Product.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['category__name']
