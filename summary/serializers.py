from django.db.models import Q
from rest_framework import serializers
from customer.models import Customer
from customer.serializers import ReducedCustomerSerializer
from order.models import Order
from order.serializers import ReducedOrderProductSerializer
from product.models import Product, ProductVersion
from shared import parameters
from shared.serializers import ReducedOrderStatusSerializer
from datetime import datetime
from django.utils import timezone
from django.utils.timezone import get_current_timezone


class CustomerOrderSummarySerializer(serializers.ModelSerializer):
    total_amount_owed = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Customer
        fields = ("total_amount_owed",)

    def get_total_amount_owed(self, customer):
        orders = customer.orders \
            .filter(~Q(status__value=parameters.ORDER_STATUS_COMPLETED)
                    & ~Q(status__value=parameters.ORDER_STATUS_ARCHIVED), is_paid=False)
        total_amount_owed = 0
        for order in orders:
            order_total_amount = order.get_total_amount()
            total_amount_owed += order_total_amount
        return total_amount_owed


class OrderSummarySerializer(serializers.ModelSerializer):
    status = ReducedOrderStatusSerializer(read_only=True)
    products = ReducedOrderProductSerializer(many=True, read_only=True)
    created = serializers.DateTimeField(read_only=True)
    last_modified = serializers.DateTimeField(read_only=True)
    total_amount = serializers.SerializerMethodField(read_only=True)
    customer = ReducedCustomerSerializer(read_only=True)

    class Meta:
        model = Order
        fields = '__all__'

    def get_total_amount(self, order):
        total_amount = order.get_total_amount()
        return total_amount


class ProductSalesSummarySerializer(serializers.ModelSerializer):
    total_ordered_quantity = serializers.SerializerMethodField(read_only=True)
    total_ordered_amount = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Product
        exclude = ("image",)

    def get_total_ordered_quantity(self, product):
        today_date = timezone.now()
        tz = get_current_timezone()
        start_date = self.context['request'].query_params.get('start_date', None)
        end_date = self.context['request'].query_params.get('end_date', None)
        order_products = []
        if start_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d')
            start_date = start_date.replace(tzinfo=tz)
            if end_date:
                end_date = datetime.strptime(end_date, '%Y-%m-%d')
                end_date = end_date.replace(tzinfo=tz)
            else:
                end_date = today_date
            product_versions = product.product_versions.all()
            # product_versions = product.product_versions.filter(created__date__lte=end_date)

            if product_versions.count():
                for product_version in product_versions:
                    for order_product in product_version.orders.filter(order__created__date__lte=end_date) \
                            .filter(order__created__gte=start_date).all():
                        order_products.append(order_product)
        else:
            product_versions = product.product_versions.all()
            if product_versions.count():
                for product_version in product_versions:
                    for order_product in product_version.orders.all():
                        order_products.append(order_product)
        total_ordered_quantity = 0
        for order_product in order_products:
            total_ordered_quantity += order_product.ordered_quantity
        return total_ordered_quantity

    def get_total_ordered_amount(self, product):
        total_ordered_quantity = self.get_total_ordered_quantity(product)
        return total_ordered_quantity * product.unit_price
