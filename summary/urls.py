from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from summary.views import CustomerOrderSummaryViewSet, OrderSummaryViewSet, ProductSalesSummaryViewSet

router = DefaultRouter()
router.register(r'customer-orders', CustomerOrderSummaryViewSet, basename='customer_order')
router.register(r'orders', OrderSummaryViewSet, basename='order_summary')
router.register(r'products', ProductSalesSummaryViewSet, basename='product_summary')


urlpatterns = [path('summaries/', include(router.urls))]
