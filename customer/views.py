from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, \
    CreateModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from customer.models import Customer, CustomerTransaction
from customer.serializers import CustomerSerializer, ReducedCustomerSerializer, CustomerTransactionSerializer
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from utils.paginations import StandardResultsSetPagination


class CustomerViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter]
    search_fields = ['first_name', 'last_name', 'title']
    pagination_class = StandardResultsSetPagination

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)


class CustomerTransactionViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    GenericViewSet,
):
    serializer_class = CustomerTransactionSerializer
    permission_classes = [IsAuthenticated]
    # filter_backends = [filters.SearchFilter]
    # search_fields = ['transaction_type']
    # pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return CustomerTransaction.objects.filter(customer__id=self.kwargs['customer_id'])

    def create(self, request, *args, **kwargs):
        request.data['initiated_by'] = {'id': self.request.user.id}
        return super().create(request, *args, **kwargs)


class ReducedCustomerViewSet(
    ListModelMixin,
    GenericViewSet,
):
    queryset = Customer.objects.all()
    serializer_class = ReducedCustomerSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)
