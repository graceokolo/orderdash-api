from decimal import Decimal

from django.db import models
from shared.models import NameTitle, PhoneNumberCountryCode, TransactionType
from utils.models import CreationDateMixin
from django.contrib.auth import get_user_model

User = get_user_model()


class Customer(CreationDateMixin, models.Model):
    title = models.ForeignKey(NameTitle, related_name='customers', on_delete=models.PROTECT)
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    email = models.EmailField(max_length=254, blank=True, null=True)
    notes = models.TextField(blank=True)
    account_balance = models.DecimalField(max_digits=12, decimal_places=2, default=0.00)

    class Meta:
        verbose_name = "Customer"
        verbose_name_plural = "Customers"
        ordering = ['-created']

    def __str__(self):
        return "{} {} {}".format(self.title.value, self.first_name, self.last_name)

    def credit_account_balance(self, amount):
        self.account_balance += amount
        super().save()
        return self.account_balance

    def debit_account_balance(self, amount):
        self.account_balance -= Decimal(amount)
        super().save()
        return self.account_balance


class CustomerTransaction(CreationDateMixin, models.Model):
    customer = models.ForeignKey(Customer, related_name='transactions', on_delete=models.PROTECT)
    transaction_type = models.ForeignKey\
        (TransactionType, related_name='customer_transactions', on_delete=models.PROTECT)
    initiated_by = models.ForeignKey(User, related_name='customer_transactions', on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    balance_at_transaction = models.DecimalField(max_digits=12, decimal_places=2, default=0.00)
    description = models.CharField(max_length=240, null=True, blank=True)

    class Meta:
        verbose_name = "Customer Transaction"
        verbose_name_plural = "Customer Transactions"
        ordering = ['-created']

    def __str__(self):
        return "{} ₦{} for {}".format(
            self.transaction_type.value, self.amount, self.customer.first_name, self.customer.last_name
        )
