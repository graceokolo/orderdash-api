# Generated by Django 3.0.5 on 2020-04-14 04:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0004_customer_title'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='nametitle',
            options={'verbose_name': 'Name Title', 'verbose_name_plural': 'Name Titles'},
        ),
    ]
