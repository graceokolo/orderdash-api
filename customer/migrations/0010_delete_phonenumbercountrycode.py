# Generated by Django 3.0.5 on 2020-04-21 10:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0009_remove_phonenumber_country_code'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PhoneNumberCountryCode',
        ),
    ]
