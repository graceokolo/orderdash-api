from rest_framework import serializers
from customer.models import Customer, CustomerTransaction
from general.serializers import ReducedUserSerializer
from shared.models import NameTitle, TransactionType
from shared.parameters import TRANSACTION_TYPE_CREDIT
from shared.serializers import NameTitleSerializer, TransactionTypeSerializer
from django.db import transaction
from django.contrib.auth import get_user_model

User = get_user_model()


class ReducedCustomerSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)

    class Meta:
        model = Customer
        fields = ('id', 'first_name', 'last_name')


class CustomerSerializer(serializers.ModelSerializer):
    title = NameTitleSerializer()
    account_balance = serializers.DecimalField(max_digits=12, decimal_places=2, read_only=True)

    class Meta:
        model = Customer
        fields = "__all__"

    def create(self, validated_data):
        title_data = validated_data.pop('title', None)
        title_instance = NameTitle.objects.get(id=title_data['id'])
        customer_instance = Customer.objects.create(
            title=title_instance,
            **validated_data
        )
        return customer_instance

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.last_name = validated_data.get("last_name", instance.last_name)
        instance.email = validated_data.get("email", instance.email)
        instance.notes = validated_data.get("notes", instance.notes)
        title_data = validated_data.pop('title', None)
        if title_data:
            title_instance = NameTitle.objects.get(id=title_data['id'])
            instance.title = title_instance
        instance.save()
        return instance


class CustomerTransactionSerializer(serializers.ModelSerializer):
    customer = ReducedCustomerSerializer(required=False)
    transaction_type = TransactionTypeSerializer(required=False)
    initiated_by = ReducedUserSerializer(required=True)

    class Meta:
        model = CustomerTransaction
        fields = "__all__"

    def validate(self, attrs):
        try:
            customer_id = self.context['request'].parser_context['kwargs']['customer_id']
            customer_instance = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            raise serializers.ValidationError("Customer matching query does not exist.")
        attrs['customer'] = customer_instance
        return attrs

    def create(self, validated_data):
        transaction_type_instance = TransactionType.objects.get(value=TRANSACTION_TYPE_CREDIT)
        initiated_by_data = validated_data.pop('initiated_by')
        initiated_by_instance = User.objects.get(id=initiated_by_data['id'])
        customer = validated_data.get('customer')

        with transaction.atomic():
            balance_at_transaction = customer.credit_account_balance(validated_data.get('amount'))
            customer_transaction_instance = CustomerTransaction.objects.create(
                transaction_type=transaction_type_instance,
                balance_at_transaction=balance_at_transaction,
                initiated_by=initiated_by_instance,
                **validated_data
            )
        return customer_transaction_instance
