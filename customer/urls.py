from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from customer.views import CustomerViewSet, ReducedCustomerViewSet, CustomerTransactionViewSet

router = DefaultRouter()
router.register(r'customers/(?P<customer_id>\d+)/transactions',
                CustomerTransactionViewSet, basename='customer-transactions')
router.register(r'customers/reduced', ReducedCustomerViewSet, basename='reduced-customer')
router.register(r'customers', CustomerViewSet, basename='customer')


urlpatterns = [path('', include(router.urls))]
