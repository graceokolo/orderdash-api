from django.contrib import admin
from customer.models import Customer, CustomerTransaction

admin.site.register(Customer)
admin.site.register(CustomerTransaction)
