import os

from django.core.files.storage import FileSystemStorage
from django.db import models
from django.utils.translation import gettext_lazy as _
from orderdash import settings
from shared.models import ProductStatus
from utils.models import CreationDateMixin


class Category(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __str__(self):
        return "{} product category".format(self.name)


def upload_to(instance, filename):
    # now = timezone.now()
    base, extension = os.path.splitext(filename.lower())
    # milliseconds = now.microsecond // 1000
    # return f"books/{instance.pk}/{now:%Y%m%d%H%M%S}{milliseconds}{extension}"
    if not extension:
        extension = ".jpg"
    return f"product_images/{instance.pk}{extension}"


class OverwriteStorage(FileSystemStorage):

    def get_available_name(self, name, max_length=None):
        # If the filename already exists, remove it as if it was a true file system
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


class Product(models.Model):
    name = models.CharField(max_length=128, null=False, unique=True)
    quantity_in_stock = models.IntegerField(default=0)
    # image = models.ImageField(upload_to='product-images', null=True)
    image = models.ImageField(_("image"), upload_to=upload_to, storage=OverwriteStorage(), null=True, blank=True)
    is_active = models.BooleanField(default=True)
    category = models.ForeignKey(Category, related_name='products', on_delete=models.PROTECT)
    unit_price = models.FloatField(default=0.0)

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"
        ordering = ['name', 'quantity_in_stock']

    # def _get_unit_price(self):
    #     """Returns the products unit price based on the active product version."""
    #     unit_price = self.product_versions.filter(is_active=True)[0].unit_price
    #     return unit_price

    # unit_price = property(_get_unit_price)

    def __str__(self):
        return "{}, {} units in stock from {} category".format(self.name, self.quantity_in_stock, self.category.name)


class ProductVersion(CreationDateMixin, models.Model):
    unit_price = models.FloatField(default=0.0)
    is_active = models.BooleanField(default=True)
    product = models.ForeignKey(Product, related_name='product_versions', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Product Version"
        verbose_name_plural = "Product Versions"
        ordering = ['is_active']

    def __str__(self):
        return "{}, {} units in stock from {} category"\
            .format(self.product.name, self.product.quantity_in_stock, self.product.category.name)
