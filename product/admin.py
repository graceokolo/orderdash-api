from django.contrib import admin
from product.models import Product, Category, ProductVersion


class ProductAdmin(admin.ModelAdmin):
    list_display = ("name", "quantity_in_stock", "is_active", "category")


admin.site.register(Product, ProductAdmin)
admin.site.register(ProductVersion)
admin.site.register(Category)
