import json

from django.core.management.base import BaseCommand
import requests
from product.models import Product
from utils.create_presigned_url import create_presigned_url


class Command(BaseCommand):
    help = 'Imports Book data from an XML file to the database.'

    def handle(self, *args, **kwargs):
        products = Product.objects.all()

        for product in products:
            image_representation = product.image.name
            s3_object_name = image_representation.split('/')[len(image_representation.split('/')) - 1]
            if '?' in s3_object_name:
                s3_object_name = s3_object_name.split('?')[0]
            img_url = create_presigned_url(s3_object_name)
            # print("img_url", img_url)

            img_data = requests.get(img_url).content
            with open(f'product_imgs/image-{product.id}.jpg', 'wb') as handler:
                handler.write(img_data)
                print("downloaded! product image", product.name)
