import json
from django.core.files.base import ContentFile, File
from django.core.management.base import BaseCommand
import requests
from product.models import Product
from utils.create_presigned_url import create_presigned_url


class Command(BaseCommand):
    help = 'Imports Book data from an XML file to the database.'

    def handle(self, *args, **kwargs):
        products = Product.objects.all()

        for product in products:
            with open(f'product_imgs/image-{product.id}.jpg', 'rb') as file:
                # handler.write(img_data)
                # self.license_file.save(new_name, File(f))
                product.image.save(f"image-{product.id}", File(file))
                print("uploaded! product image", product.name)
