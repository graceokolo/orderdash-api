# Generated by Django 3.0.4 on 2020-04-06 11:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0007_product_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterUniqueTogether(
            name='product',
            unique_together={('name', 'is_active')},
        ),
    ]
