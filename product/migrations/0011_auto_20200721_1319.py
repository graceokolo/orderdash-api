# Generated by Django 3.0.7 on 2020-07-21 10:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0010_product_category'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='product',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='product',
            name='unit_price',
        ),
    ]
