from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from .views import ProductViewSet, CategoryViewSet

router = DefaultRouter()
router.register(r'categories', CategoryViewSet, basename='category')
router.register(r'', ProductViewSet, basename='product')


urlpatterns = [path('products/', include(router.urls))]
