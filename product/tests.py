from product.models import Product
import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse

# initialize the APIClient app
from product.serializers import ProductSerializer

client = Client()


class ProductAPITest(TestCase):
    """ Test module for all product API """
    def setUp(self):
        self.product1 = Product.objects.create(
            name='Product1', quantity=1)
        self.product2 = Product.objects.create(
            name='Product2', quantity=2)
        self.product3 = Product.objects.create(
            name='Product3', quantity=3)
        self.product4 = Product.objects.create(
            name='Product4', quantity=4)

        self.valid_payload = {
            'name': 'Poundo Yam',
            'quantity': 4
        }
        self.invalid_payload = {
            'name': '',
            'quantity': 4
        }

    def test_get_all_products(self):
        # get API response
        response = client.get(reverse('product-list'))
        # get data from db
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_product(self):
        response = client.get(
            reverse('product-detail', kwargs={'pk': self.product1.pk}))
        product = Product.objects.get(pk=self.product1.pk)
        serializer = ProductSerializer(product)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_product(self):
        response = client.get(
            reverse('product-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_valid_product(self):
        response = client.post(
            reverse('product-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_product(self):
        response = client.post(
            reverse('product-list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_update_product(self):
        response = client.patch(
            reverse('product-detail', kwargs={'pk': self.product1.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_product(self):
        response = client.patch(
            reverse('product-detail', kwargs={'pk': self.product1.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_delete_product(self):
        response = client.delete(
            reverse('product-detail', kwargs={'pk': self.product1.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_product(self):
        response = client.delete(
            reverse('product-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class ModelProductTestCase(TestCase):
    """
    This class defines the test suite for the model Product.
    CRUD
    """

    def setUp(self):
        """Define the test client and other test variables."""
        self.product_name = "Product 1"
        self.product_quantity = 5
        self.product = Product(name=self.product_name, quantity=self.product_quantity)

    def test_model_can_create_a_product(self):
        """Test the Product model can create a product object."""
        old_count = Product.objects.count()
        self.product.save()
        new_count = Product.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_can_update_a_product(self):
        """
        Test the Product model can update a product object
        """
        self.product.save()
        self.product.name = "Product 1 updated"
        self.assertNotEquals(self.product.name, self.product_name)

    def test_model_can_delete_a_product(self):
        """
        Test the Product model can delete a Product object
        """
        self.product.save()
        product_count = Product.objects.all().count()
        self.product.delete()
        product_count_new = Product.objects.all().count()
        self.assertNotEquals(product_count, product_count_new)

    def test_model_can_retrieve_one_object(self):
        """
        Test the Product model can retrieve a Product object
        """
        self.product.save()
        product_id = self.product.id
        saved_product = Product.objects.get(id=product_id)
        self.assertEquals(saved_product.name, self.product.name)

    def test_model_can_retrieve_multiple_objects(self):
        """
        Test the Product model can retrieve multiple Product object
        """
        self.product.save()
        Product(name="Product 2").save()
        Product(name="Product 3").save()
        self.assertEquals(Product.objects.all().count(), 3)
