import json
from rest_framework import serializers
from product.models import Product, Category, ProductVersion
# from utils.create_presigned_url import create_presigned_url
import base64
from django.core.files.base import ContentFile


class ReducedProductVersionSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    name = serializers.CharField(source="product.name")

    class Meta:
        model = ProductVersion
        fields = ("id", "name", "unit_price")


class ReducedCategorySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Category
        fields = ("id", "name")


class CategorySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Category
        fields = "__all__"

    def create(self, validated_data):
        category_instance = Category.objects.create(
            **validated_data
        )
        return category_instance

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.save()
        return instance


class ProductSerializer(serializers.ModelSerializer):
    is_active = serializers.BooleanField(default=True)

    class Meta:
        model = Product
        fields = "__all__"

    def create(self, validated_data):
        product_instance = Product.objects.create(
            **validated_data
        )
        return product_instance

    def to_representation(self, instance):
        """
        send a pre-signed URL of the image saved on S3
        """
        try:
            representation = super().to_representation(instance)
            # image_representation = representation.pop("image")
            # s3_object_name = image_representation.split('/')[len(image_representation.split('/')) - 1]
            # if '?' in s3_object_name:
            #     s3_object_name = s3_object_name.split('?')[0]
            # url = create_presigned_url(s3_object_name)
            # if url is not None:
            #     representation["image"] = url
            # else:
            #     representation["image"] = ''
        except AttributeError:
            pass
        category = Category.objects.get(id=representation['category'])
        representation['category'] = {'id': category.id, 'name': category.name}
        return representation

    def to_internal_value(self, data):
        """
        Converts base64 image to an image file
        """
        data_copy = data.copy()
        category_data = json.loads(data_copy.get("category"))
        try:
            image_base64_data = data_copy.get("image")
            data_copy.pop("image")
        except KeyError:
            pass
        else:
            try:
                img_format, img_string = image_base64_data.split(';base64,')
                img_ext = img_format.split('/')[-1]
                img_data = ContentFile(base64.b64decode(img_string), name='temp.' + img_ext)
                data_copy['image'] = img_data
            except AttributeError:
                pass
        if len(category_data) > 0:
            data_copy['category'] = category_data['id']
        else:
            data_copy.pop('category')
        internal = super().to_internal_value(data_copy)
        return internal

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.is_active = validated_data.get("is_active", instance.is_active)
        instance.quantity_in_stock = validated_data.get("quantity_in_stock", instance.quantity_in_stock)
        instance.image = validated_data.get("image", instance.image)
        instance.category = validated_data.get("category", instance.category)
        instance.unit_price = validated_data.get("unit_price", instance.unit_price)
        instance.save()
        return instance
