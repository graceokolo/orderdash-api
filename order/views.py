import datetime
from django.utils.datastructures import MultiValueDictKeyError
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from customer.models import Customer
from order.models import Order
from order.serializers import OrderSerializer
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
from shared import parameters


class OrderViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['status__value', 'is_paid', 'customer__id']

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @action(methods=['patch'], detail=True, url_path='change-status', url_name='change_status')
    def change_status(self, request, pk=None):
        order = self.get_object()
        serializer = OrderSerializer(data=request.data, partial=True)
        if serializer.is_valid():
            try:
                request.data['status']['id']
            except KeyError:
                return Response({'Validation error': 'status is required'}, status=status.HTTP_400_BAD_REQUEST)
            order.set_status(request.data['status']['id'])
            order.save()
            return Response()
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['get'], detail=False, url_path='recently-active', url_name='recently_active')
    def recently_active(self, request, pk=None):
        today = datetime.date.today()
        previous_2_days = today - datetime.timedelta(days=2)
        queryset = Order.objects.filter(created__date__range=(previous_2_days, today))
        # queryset = Order.objects.filter()
        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=False, url_path='in-progress', url_name='in_progress')
    def in_progress(self, request, pk=None):
        try:
            customer_instance = Customer.objects.get(pk=request.query_params['customer'])
            queryset = Order.objects.filter(
                ~Q(status__value=parameters.ORDER_STATUS_COMPLETED) & ~Q(status__value=parameters.ORDER_STATUS_ARCHIVED),
                customer=customer_instance
            )
        except Customer.DoesNotExist:
            raise ValidationError(
                "Validation error: Customer does not exist"
            )
        except MultiValueDictKeyError:
            queryset = Order.objects.filter(
                ~Q(status__value=parameters.ORDER_STATUS_COMPLETED) & ~Q(status__value=parameters.ORDER_STATUS_ARCHIVED)
            )
        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['patch'], detail=True, url_path='archive-order', url_name='archive-order')
    def archive_order(self, request, pk=None):
        order = self.get_object()
        serializer = OrderSerializer(data=request.data, partial=True)
        if serializer.is_valid():
            try:
                request.data['archive_note']
            except KeyError:
                return Response({'Validation error': 'archive_note is required'}, status=status.HTTP_400_BAD_REQUEST)
            order.archive_order(request.data['archive_note'])
            order.save()
            return Response()
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
