# Generated by Django 3.0.7 on 2020-07-21 10:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0012_productversion'),
        ('order', '0034_auto_20200609_1816'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderproduct',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='orders', to='product.ProductVersion'),
        ),
    ]
