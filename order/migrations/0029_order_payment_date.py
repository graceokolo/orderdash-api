# Generated by Django 3.0.5 on 2020-04-16 20:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0028_order_customer'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='payment_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='payment date and time'),
        ),
    ]
