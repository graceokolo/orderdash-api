# Generated by Django 3.0.5 on 2020-05-06 20:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0031_order_customer'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='charge_customer',
            field=models.BooleanField(default=False),
        ),
    ]
