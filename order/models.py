from django.db import models

from customer.models import Customer
from product.models import ProductVersion
from shared.models import OrderStatus
from shared.parameters import ORDER_STATUS_ARCHIVED
from table.models import Table
from utils.models import ModificationDateMixin, CreationDateMixin


class Order(CreationDateMixin, ModificationDateMixin, models.Model):
    """
    Model representing a purchase order
    including fields created and last_modified
    """

    order_number = models.CharField(max_length=20, null=False, unique=True)  # make unique
    table = models.CharField(max_length=20, null=False)  # reduce size
    notes = models.CharField(blank=True, max_length=360)
    status = models.ForeignKey(OrderStatus, related_name="orders", on_delete=models.PROTECT)
    archive_note = models.CharField(blank=True, null=True, max_length=260)
    is_paid = models.BooleanField(default=False)
    charge_customer = models.BooleanField(default=False)
    customer = models.ForeignKey(Customer, related_name="orders", on_delete=models.SET_NULL, null=True, blank=True)
    payment_date = models.DateTimeField(
        verbose_name="payment date and time", null=True, blank=True
    )

    class Meta:
        verbose_name = "Order"
        verbose_name_plural = "Orders"
        ordering = ['-created', '-last_modified']

    def __str__(self):
        return "Order for {}, created on {} and last updated on {}".format(self.table, self.created, self.last_modified)

    def get_total_amount(self):
        order_total_amount = 0
        order_products = OrderProduct.objects.filter(order=self)
        for order_product in order_products:
            product_cost = order_product.product.unit_price
            order_total_amount += (product_cost * order_product.ordered_quantity)
        return order_total_amount

    def set_status(self, status_id):
        self.status = OrderStatus.objects.get(id=status_id)

    def archive_order(self, archive_note):
        self.archive_note = archive_note
        self.status = OrderStatus.objects.get(value=ORDER_STATUS_ARCHIVED)


class OrderProduct(models.Model):
    """
    Link model for the many-to-many relationship between order and product models
    """
    order = models.ForeignKey(Order, related_name="products", on_delete=models.CASCADE)
    product = models.ForeignKey(ProductVersion, related_name="orders", on_delete=models.CASCADE)
    ordered_quantity = models.IntegerField(default=0)

    class Meta:
        verbose_name = "OrderProduct"
        verbose_name_plural = "OrderProducts"
        unique_together = ['order', 'product']

    def __str__(self):
        return "Product: {} in Order: ID{}".format(self.product.product.name, self.order.id)
