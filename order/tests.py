import json

from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from product.models import Product
from .models import Order, OrderProduct
from .serializers import OrderSerializer

client = Client()


class OrderAPITest(TestCase):
    """
    Test module for all product API
    """

    def setUp(self):
        self.product1 = Product.objects.create(
            name='Product1', quantity_in_stock=1)
        self.product2 = Product.objects.create(
            name='Product2', quantity_in_stock=2)
        self.product3 = Product.objects.create(
            name='Product3', quantity_in_stock=3)
        self.order1 = Order()
        self.order1.save()
        self.order2 = Order()
        self.order2.save()
        self.order = Order.objects.create(
            notes='food order')
        self.order.save()
        OrderProduct(order=self.order, product=self.product1, ordered_quantity=5)
        OrderProduct(order=self.order, product=self.product2, ordered_quantity=5)
        OrderProduct(order=self.order, product=self.product3, ordered_quantity=5)

        self.valid_payload = {
            "products": [
                {
                    "product": {
                        "id": self.product1.id
                    },
                    "ordered_quantity": 2
                },
                {
                    "product": {
                        "id": self.product2.id
                    },
                    "ordered_quantity": 2
                }
            ],
            "notes": "Not so spicy"
        }
        self.invalid_payload = {
            "products": [
                {
                    "ordered_quantity": 2
                }
            ]
        }

    def test_get_all_orders(self):
        # get API response
        response = client.get(reverse('order-list'))
        # get data from db
        orders = Order.objects.all()
        serializer = OrderSerializer(orders, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_order(self):
        response = client.get(
            reverse('order-detail', kwargs={'pk': self.order1.pk}))
        order = Order.objects.get(pk=self.order1.pk)
        serializer = OrderSerializer(order)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_order(self):
        response = client.get(
            reverse('order-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_valid_order(self):
        response = client.post(
            reverse('order-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_order(self):
        response = client.post(
            reverse('order-list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_update_product(self):
        response = client.patch(
            reverse('product-detail', kwargs={'pk': self.product1.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # def test_invalid_update_product(self):
    #     response = client.patch(
    #         reverse('product-detail', kwargs={'pk': self.product1.pk}),
    #         data=json.dumps(self.invalid_payload),
    #         content_type='application/json')
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_valid_delete_product(self):
        response = client.delete(
            reverse('product-detail', kwargs={'pk': self.product1.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_product(self):
        response = client.delete(
            reverse('product-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class ModelOrderTestCase(TestCase):
    """
    This class defines the test suite for the model Order.
    CRUD
    """

    def setUp(self):
        """Define the test client and other test variables."""
        self.order_name = "Order 1"
        self.order = Order()

    def test_model_can_create_an_order(self):
        """Test the Order model can create a order object."""
        old_count = Order.objects.count()
        self.order.save()
        new_count = Order.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_can_update_an_order(self):
        """
        Test the Order model can update an order object
        """
        product_one = Product(name="Product 1")
        product_one.save()
        product_two = Product(name="Product 2")
        product_two.save()
        new_order = Order()
        new_order.save()
        order_product1 = OrderProduct(order=new_order, product=product_one)
        order_product1.save()
        order_product2 = OrderProduct(order=new_order, product=product_two)
        order_product2.save()
        self.assertEquals(OrderProduct.objects.filter(order__id=new_order.id).count(), 2)

    def test_model_can_delete_an_order(self):
        """
        Test the Order model can delete an order object
        """
        self.order.save()
        order_count = Order.objects.all().count()
        self.order.delete()
        order_count_new = Order.objects.all().count()
        self.assertNotEquals(order_count, order_count_new)

    def test_model_can_retrieve_one_object(self):
        """
        Test the Order model can retrieve a order object
        """
        self.order.save()
        order_id = self.order.id
        saved_order = Order.objects.get(id=order_id)
        self.assertEquals(saved_order.id, self.order.id)
