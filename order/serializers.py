import random

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from customer.models import Customer, CustomerTransaction
from customer.serializers import ReducedCustomerSerializer
from order.models import Order, OrderProduct
from product.models import Product, ProductVersion
from django.db import transaction
from rest_framework.exceptions import ValidationError
from product.serializers import ReducedProductVersionSerializer
from shared.models import OrderStatus, TransactionType
from shared.parameters import ORDER_STATUS_OPEN, ORDER_STATUS_COMPLETED, TRANSACTION_TYPE_DEBIT
from shared.serializers import ReducedOrderStatusSerializer
from datetime import date
from django.utils import timezone


class ReducedOrderProductSerializer(serializers.ModelSerializer):
    # id = serializers.ModelField(model_field=OrderProduct()._meta.get_field('id'))
    product = ReducedProductVersionSerializer(required=True)
    ordered_quantity = serializers.IntegerField(required=True)

    class Meta:
        model = OrderProduct
        exclude = ('order', 'id',)


class OrderSerializer(serializers.ModelSerializer):
    status = ReducedOrderStatusSerializer(required=False)
    products = ReducedOrderProductSerializer(many=True, required=False)
    created = serializers.DateTimeField(read_only=True)
    last_modified = serializers.DateTimeField(read_only=True)
    total_amount = serializers.SerializerMethodField(read_only=True)
    customer = ReducedCustomerSerializer(required=False)
    order_number = serializers.CharField(read_only=True)

    class Meta:
        model = Order
        fields = '__all__'

    def validate(self, data):
        """
        Block completing an unpaid order
        """
        try:
            order_object = Order.objects.get(id=self.initial_data['id'])
            completed_status = OrderStatus.objects.get(value=ORDER_STATUS_COMPLETED)

            if data['status']['id'] == completed_status.id:
                if not order_object.is_paid:
                    raise serializers.ValidationError("You cannot complete an unpaid order")
        except KeyError:
            pass
        return data

    def get_total_amount(self, order):
        total_amount = order.get_total_amount()
        return total_amount

    def validate(self, attrs):
        customer_data = attrs.pop('customer', None)
        if customer_data:
            try:
                customer_id = customer_data['id']
                customer_instance = Customer.objects.get(id=customer_id)
            except Customer.DoesNotExist:
                raise serializers.ValidationError("Customer does not exist.")
            attrs['customer'] = customer_instance

        charge_customer = attrs.get("charge_customer", None)
        if charge_customer:
            order_id = self.context['request'].parser_context['kwargs']['pk']
            customer = Order.objects.get(pk=order_id).customer
            if customer:
                pass
            else:
                raise serializers.ValidationError("Assign a customer to charge this order to.")
        return attrs

    def create(self, validated_data):
        products_data = validated_data.pop("products", [])
        status = OrderStatus.objects.get(value=ORDER_STATUS_OPEN)
        today = date.today()
        order_number = "OR{}{}{}{}" \
            .format(f'{today.day :02}', f'{today.month :02}', str(today.year)[2:4], random.randint(10, 99))

        with transaction.atomic():
            order_instance = Order.objects.create(
                status=status,
                order_number=order_number,
                **validated_data
            )

            for data_point in products_data:
                product_version = ProductVersion.objects.get(product__id=data_point["product"]["id"], is_active=True)

                if product_version.product.quantity_in_stock < data_point["ordered_quantity"]:
                    raise ValidationError(
                        "Validation error: You are attempting to order more than the available quantity. "
                        "The product {} has {} units in stock"
                            .format(product_version.product.name, product_version.product.quantity_in_stock)
                    )
                ordered_quantity = data_point["ordered_quantity"]
                OrderProduct.objects.create(
                    order=order_instance, product=product_version, ordered_quantity=ordered_quantity
                )

        return order_instance

    def update(self, instance, validated_data):
        instance.notes = validated_data.get("notes", instance.notes)
        instance.table = validated_data.get("table", instance.table)
        instance.customer = validated_data.get("customer", instance.customer)
        products_data = validated_data.pop("products", None)
        status_data = validated_data.pop("status", None)

        if status_data:
            instance.status = OrderStatus.objects.get(id=status_data['id'])

        with transaction.atomic():
            charge_customer = validated_data.get("charge_customer", None)
            if charge_customer:
                if instance.charge_customer:
                    pass
                else:
                    customer = validated_data.get("customer", instance.customer)
                    order_total_amount = instance.get_total_amount()
                    instance.is_paid = True
                    instance.payment_date = timezone.now()
                    instance.charge_customer = True
                    # create a debit transaction
                    balance_at_transaction = customer.debit_account_balance(order_total_amount)
                    initiated_by = self.context['request'].user
                    transaction_type = TransactionType.objects.get(value=TRANSACTION_TYPE_DEBIT)
                    description = "Order {} created at {}".format(
                        instance.order_number, instance.created.strftime("%b %d %Y")
                    )
                    CustomerTransaction.objects.create(
                        customer=customer,
                        transaction_type=transaction_type,
                        initiated_by=initiated_by,
                        amount=order_total_amount,
                        balance_at_transaction=balance_at_transaction,
                        description=description
                    )

            current_order_products = instance.products
            if products_data is not None:
                for data_point in products_data:
                    try:
                        product_version: ProductVersion = ProductVersion.objects.get(
                            product__name=data_point["product"]["product"]["name"],
                            is_active=True
                        )
                        ordered_quantity = data_point["ordered_quantity"]
                    except KeyError:
                        raise ValidationError("Validation error: product and quantity are both required.")

                    try:
                        current_order_products.get(
                            product=product_version,
                            order=instance,
                            ordered_quantity=ordered_quantity
                        )
                    except ObjectDoesNotExist:
                        try:
                            old_ordered_quantity = current_order_products.get(
                                product=product_version,
                                order=instance
                            ).ordered_quantity
                            current_product_quantity_in_stock = product_version.product.quantity_in_stock
                            previous_product_quantity = current_product_quantity_in_stock + old_ordered_quantity
                            if previous_product_quantity < ordered_quantity:
                                raise ValidationError(
                                    "Validation error: You are attempting to order more than the available quantity. "
                                    "The product {} has {} units in stock".format(
                                        product_version.product.name,
                                        product_version.product.quantity_in_stock
                                    )
                                )
                            Product.objects.filter(pk=product_version.product.id) \
                                .update(quantity_in_stock=(previous_product_quantity - ordered_quantity))
                            OrderProduct.objects.filter(product=product_version, order=instance).update(
                                ordered_quantity=ordered_quantity
                            )
                        except ObjectDoesNotExist:
                            OrderProduct.objects.create(
                                product=product_version,
                                order=instance,
                                ordered_quantity=ordered_quantity
                            )
                    current_order_products = current_order_products.exclude(
                        product=product_version, order=instance, ordered_quantity=ordered_quantity
                    )
                if current_order_products.count() > 0:
                    for order_product in current_order_products.all():
                        ordered_quantity = order_product.ordered_quantity
                        product_quantity_in_stock = order_product.product.product.quantity_in_stock
                        Product.objects.filter(pk=order_product.product.product.id) \
                            .update(quantity_in_stock=(product_quantity_in_stock + ordered_quantity))
                    current_order_products.all().delete()

        if not instance.charge_customer:
            instance.is_paid = validated_data.get("is_paid", instance.is_paid)
            instance.payment_date = timezone.now()

        instance.save()
        return instance
