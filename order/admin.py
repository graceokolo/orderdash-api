from django.contrib import admin
from order.models import Order, OrderProduct


class OrderAdmin(admin.ModelAdmin):
    list_display = ("table", "created", "last_modified", "is_paid")


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderProduct)
