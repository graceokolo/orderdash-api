# Generated by Django 3.0.3 on 2020-02-15 22:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('table', '0002_auto_20200215_2248'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='table',
            options={'ordering': ['name'], 'verbose_name': 'Table', 'verbose_name_plural': 'Tables'},
        ),
    ]
