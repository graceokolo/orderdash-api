from django.db import models
from shared.models import TableStatus


class Table(models.Model):
    name = models.CharField(max_length=128, null=False, unique=True)
    status = models.ForeignKey(TableStatus, related_name="orders", on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Table"
        verbose_name_plural = "Tables"
        ordering = ['name']

    def __str__(self):
        return "{} is {}".format(self.name, self.status)
