from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from .views import TableViewSet


router = DefaultRouter()
router.register(r'', TableViewSet, basename='table')


urlpatterns = [path('tables/', include(router.urls))]
