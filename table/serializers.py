from rest_framework import serializers
from table.models import Table


class ReducedTableSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    name = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = Table
        fields = ("id", "name")


class TableSerializer(serializers.ModelSerializer):

    class Meta:
        model = Table
        fields = "__all__"

    def create(self, validated_data):
        table_instance = Table.objects.create(
            **validated_data
        )
        return table_instance

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        return instance
