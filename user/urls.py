from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from .views import ProfileViewSet

router = DefaultRouter()
router.register(r'', ProfileViewSet, basename='user')


urlpatterns = [path('users/', include(router.urls))]
