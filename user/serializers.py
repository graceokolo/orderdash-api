from django.contrib.auth.models import User, Group
from rest_framework import serializers

from user.models import Profile, Department


class DepartmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Department
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source="user.username")
    first_name = serializers.CharField(source="user.first_name")
    last_name = serializers.CharField(source="user.last_name")
    email = serializers.EmailField(source="user.email")
    department = DepartmentSerializer()

    class Meta:
        model = Profile
        exclude = ['user']
