from django.contrib import admin
from user.models import Department, Profile

admin.site.register(Profile)
admin.site.register(Department)
