# Generated by Django 3.0.5 on 2020-04-21 20:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shared', '0007_phonenumbercountrycode'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='orderstatus',
            options={'ordering': ['id'], 'verbose_name': 'Order Status', 'verbose_name_plural': 'Order Status'},
        ),
        migrations.AlterModelOptions(
            name='productstatus',
            options={'ordering': ['value'], 'verbose_name': 'Product Status', 'verbose_name_plural': 'Product Status'},
        ),
        migrations.AlterModelOptions(
            name='tablestatus',
            options={'ordering': ['value'], 'verbose_name': 'Table Status', 'verbose_name_plural': 'Table Status'},
        ),
    ]
