# Generated by Django 3.0.3 on 2020-03-02 10:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shared', '0002_tablestatus'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderstatus',
            name='type',
            field=models.CharField(choices=[('AUTO', 'Automatic'), ('MAN', 'Manual')], default='AUTO', max_length=5),
        ),
    ]
