from rest_framework import serializers
from shared.models import OrderStatus, TableStatus, NameTitle, PhoneNumberCountryCode, Country, TransactionType


class ReducedOrderStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = OrderStatus
        fields = ("id", "value")


class ReducedTableStatusSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(required=False, read_only=True)

    class Meta:
        model = TableStatus
        fields = ("id", "value")


class NameTitleSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(read_only=True)

    class Meta:
        model = NameTitle
        fields = ("id", "value")


class PhoneNumberCountryCodeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    code = serializers.CharField(required=False)
    country = serializers.CharField(read_only=True, required=False)

    class Meta:
        model = PhoneNumberCountryCode
        fields = "__all__"


class CountrySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    country = serializers.CharField(read_only=True, required=False)

    class Meta:
        model = Country
        fields = "__all__"


class TransactionTypeSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    value = serializers.CharField(read_only=True)

    class Meta:
        model = TransactionType
        fields = ("id", "value")
