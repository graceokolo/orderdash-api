# Region Order Status
ORDER_STATUS_OPEN = 'open'
ORDER_STATUS_PROCESSING = 'processing'
ORDER_STATUS_AWAITING_PAYMENT = 'awaitingPayment'
ORDER_STATUS_COMPLETED = 'completed'
ORDER_STATUS_READY = 'ready'
ORDER_STATUS_SERVED = 'served'
ORDER_STATUS_ARCHIVED = 'archived'

# Region TableStatus
TABLE_STATUS_OCCUPIED = 'occupied'
TABLE_STATUS_VACANT = 'vacant'


# Region Departments
DEPARTMENT_KITCHEN = 'Kitchen'
DEPARTMENT_BAR = 'Bar'
DEPARTMENT_ADMINISTRATION = 'Administration'


# Region Customer Transaction Types
TRANSACTION_TYPE_CREDIT = 'credit'
TRANSACTION_TYPE_DEBIT = 'debit'


# Region Phone Number Country Code
COUNTRY_CODE_NIGERIA = 'NG'
