from rest_framework.routers import DefaultRouter
from shared.views import OrderStatusViewSet, TableStatusViewSet, NameTitleViewSet, \
    PhoneNumberCountryCodeViewSet, CountryViewSet, TransactionTypeViewSet

router = DefaultRouter()
router.register(r'order-status', OrderStatusViewSet, basename='order_status')
router.register(r'table-status', TableStatusViewSet, basename='table_status')
router.register(r'name-titles', NameTitleViewSet, basename='name_title')
router.register(r'country-codes', PhoneNumberCountryCodeViewSet, basename='country_code')
router.register(r'countries', CountryViewSet, basename='country')
router.register(r'transaction-types', TransactionTypeViewSet, basename='transaction_types')

urlpatterns = router.urls
