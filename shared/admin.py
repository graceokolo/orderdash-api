from django.contrib import admin
from shared.models import OrderStatus, TableStatus, PhoneNumberCountryCode, NameTitle, Country, TransactionType

admin.site.register(OrderStatus)
admin.site.register(TableStatus)
admin.site.register(PhoneNumberCountryCode)
admin.site.register(NameTitle)
admin.site.register(Country)
admin.site.register(TransactionType)
