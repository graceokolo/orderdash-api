from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet

from shared.models import OrderStatus, TableStatus, NameTitle, PhoneNumberCountryCode, Country, TransactionType
from shared.serializers import ReducedOrderStatusSerializer, ReducedTableStatusSerializer, NameTitleSerializer, \
    PhoneNumberCountryCodeSerializer, CountrySerializer, TransactionTypeSerializer


class OrderStatusViewSet(
    ListModelMixin,
    GenericViewSet,
):
    serializer_class = ReducedOrderStatusSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return OrderStatus.objects.filter(type='MAN').all()


class TableStatusViewSet(
    ListModelMixin,
    GenericViewSet,
):
    queryset = TableStatus.objects.all()
    serializer_class = ReducedTableStatusSerializer
    permission_classes = [IsAuthenticated]


class NameTitleViewSet(
    ListModelMixin,
    GenericViewSet,
):
    serializer_class = NameTitleSerializer
    permission_classes = [IsAuthenticated]
    queryset = NameTitle.objects.all()


class PhoneNumberCountryCodeViewSet(
    ListModelMixin,
    GenericViewSet,
):
    serializer_class = PhoneNumberCountryCodeSerializer
    permission_classes = [IsAuthenticated]
    queryset = PhoneNumberCountryCode.objects.all()


class CountryViewSet(
    ListModelMixin,
    GenericViewSet,
):
    serializer_class = CountrySerializer
    permission_classes = [IsAuthenticated]
    queryset = Country.objects.all()


class TransactionTypeViewSet(
    ListModelMixin,
    GenericViewSet,
):
    serializer_class = TransactionTypeSerializer
    permission_classes = [IsAuthenticated]
    queryset = TransactionType.objects.all()

