from product.models import Product, ProductVersion


# ensure every product has a product version
def seed_product_version():
    print('running seed_product_version now')
    all_products = Product.objects.filter(is_active=True)
    for product in all_products:
        if not len(product.product_versions.all()):
            ProductVersion.objects.create(product=product, unit_price=product.unit_price)


# seed_product_version()
