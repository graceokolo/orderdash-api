from django.db import models
from django_countries.fields import CountryField

ORDER_STATUS_TYPE = (
    ('AUTO', 'Automatic'),
    ('MAN', 'Manual')
)


class OrderStatus(models.Model):
    value = models.CharField(unique=True, max_length=128)
    type = models.CharField(
        max_length=5,
        choices=ORDER_STATUS_TYPE,
        default='AUTO'
    )

    class Meta:
        verbose_name = "Order Status"
        verbose_name_plural = "Order Status"
        ordering = ['id']

    def __str__(self):
        return "{} of type {}".format(self.value, self.type)


class TableStatus(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "Table Status"
        verbose_name_plural = "Table Status"
        ordering = ['value']

    def __str__(self):
        return "{}".format(self.value)


class ProductStatus(models.Model):
    value = models.CharField(unique=True, max_length=128)

    class Meta:
        verbose_name = "Product Status"
        verbose_name_plural = "Product Status"
        ordering = ['value']

    def __str__(self):
        return "{}".format(self.value)


class NameTitle(models.Model):
    value = models.CharField(unique=True, max_length=120)

    class Meta:
        verbose_name = "Name Title"
        verbose_name_plural = "Name Titles"

    def __str__(self):
        return "{}".format(self.value)


class PhoneNumberCountryCode(models.Model):
    code = models.CharField(unique=True, max_length=10)
    country = CountryField(blank_label="(select country)")

    class Meta:
        verbose_name = "Phone Number Country Code"
        verbose_name_plural = "Phone Number Country Codes"

    def __str__(self):
        return self.code


class Country(models.Model):
    value = CountryField(blank_label="(select country)")

    class Meta:
        verbose_name = "Country"
        verbose_name_plural = "Countries"

    def __str__(self):
        return self.value.code


class TransactionType(models.Model):
    value = models.CharField(unique=True, max_length=120)

    class Meta:
        verbose_name = "Transaction Type"
        verbose_name_plural = "Transaction Types"

    def __str__(self):
        return "{}".format(self.value)
