from rest_framework.routers import DefaultRouter
from django.urls import path
from django.conf.urls import include
from general.views import CustomerPhoneNumberViewSet, CustomerAddressViewSet, AddressViewSet

router = DefaultRouter()
router.register(r'addresses', AddressViewSet, basename='address')
router.register(r'customers/(?P<customer_id>\d+)/phone-numbers',
                CustomerPhoneNumberViewSet, basename='customer_phone_number')
router.register(r'customers/(?P<customer_id>\d+)/addresses', CustomerAddressViewSet, basename='customer_address')

urlpatterns = [path('', include(router.urls))]
