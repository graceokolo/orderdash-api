from django.db import transaction
from rest_framework import serializers
from customer.models import Customer
from general.models import PhoneNumber, CustomerPhoneNumber, Address, CustomerAddress
from shared.models import PhoneNumberCountryCode
from shared.parameters import COUNTRY_CODE_NIGERIA
from shared.serializers import PhoneNumberCountryCodeSerializer
from django.contrib.auth import get_user_model

User = get_user_model()


class ReducedUserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    username = serializers.CharField(read_only=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')


class PhoneNumberSerializer(serializers.ModelSerializer):
    country_code = PhoneNumberCountryCodeSerializer(required=False)

    class Meta:
        model = PhoneNumber
        fields = "__all__"


class CustomerPhoneNumberSerializer(serializers.ModelSerializer):
    phone_number = PhoneNumberSerializer(required=True)

    class Meta:
        model = CustomerPhoneNumber
        exclude = ('customer', )

    def validate(self, attrs):
        try:
            customer_id = self.context['request'].parser_context['kwargs']['customer_id']
            customer_instance = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            raise serializers.ValidationError("Customer matching query does not exist.")
        attrs['customer'] = customer_instance
        return attrs

    def create(self, validated_data):
        phone_number_data = validated_data.pop('phone_number', None)
        country_code_data = phone_number_data.pop('country_code', None)
        if country_code_data:
            country_code_instance = PhoneNumberCountryCode.objects.get(id=country_code_data['id'])
        else:
            country_code_instance = PhoneNumberCountryCode.objects.get(country=COUNTRY_CODE_NIGERIA)

        with transaction.atomic():
            phone_number_instance = PhoneNumber.objects.create(
                value=phone_number_data.get('value'),
                country_code=country_code_instance
            )
            customer_phone_number_instance = CustomerPhoneNumber.objects.create(
                phone_number=phone_number_instance,
                **validated_data
            )
        return customer_phone_number_instance


class ReducedPhoneNumberSerializer(serializers.ModelSerializer):
    country_code = PhoneNumberCountryCodeSerializer()

    class Meta:
        model = PhoneNumber
        exclude = ('customer', )


class AddressSerializer(serializers.ModelSerializer):
    country = serializers.CharField(required=True)

    class Meta:
        model = Address
        fields = "__all__"

    def update(self, instance, validated_data):
        instance.house = validated_data.get('house', instance.house)
        instance.street = validated_data.get('street', instance.street)
        instance.district = validated_data.get('district', instance.district)
        instance.city = validated_data.get('city', instance.city)
        instance.zip_code = validated_data.get('zip_code', instance.zip_code)
        instance.country = validated_data.get('country', instance.country)
        instance.save()
        return instance


class CustomerAddressSerializer(serializers.ModelSerializer):
    address = AddressSerializer(required=True)

    class Meta:
        model = CustomerAddress
        exclude = ('customer', )

    def validate(self, attrs):
        try:
            customer_id = self.context['request'].parser_context['kwargs']['customer_id']
            customer_instance = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            raise serializers.ValidationError("Customer matching query does not exist.")
        attrs['customer'] = customer_instance
        return attrs

    def create(self, validated_data):
        address_data = validated_data.pop('address')
        with transaction.atomic():
            address_instance = Address.objects.create(
                **address_data
            )
            customer_address_instance = CustomerAddress.objects.create(
                address=address_instance,
                **validated_data
            )
        return customer_address_instance


class ReducedAddressSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Address
        exclude = ('id', )
