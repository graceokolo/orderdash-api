from django.db import models
from customer.models import Customer
from shared.models import PhoneNumberCountryCode
from django_countries.fields import CountryField

from utils.models import CreationDateMixin


class PhoneNumber(models.Model):
    value = models.CharField(max_length=32)
    country_code = models.ForeignKey(
        PhoneNumberCountryCode, related_name="phone_numbers", on_delete=models.PROTECT
    )

    class Meta:
        verbose_name = "Phone Number"
        verbose_name_plural = "Phone Numbers"

    def __str__(self):
        return "{} {}".format(self.country_code, self.value)


class CustomerPhoneNumber(models.Model):
    customer = models.ForeignKey(Customer, related_name='phone_numbers', on_delete=models.CASCADE)
    phone_number = models.OneToOneField(PhoneNumber, related_name='customer', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Customer Phone Number"
        verbose_name_plural = "Customer Phone Numbers"

    def __str__(self):
        return "{} {} for {} {}"\
            .format(self.phone_number.country_code, self.phone_number.value,
                    self.customer.first_name, self.customer.last_name)


class Address(CreationDateMixin, models.Model):
    house = models.CharField(max_length=120)
    street = models.CharField(max_length=120)
    district = models.CharField(max_length=120, null=True, blank=True)
    city = models.CharField(max_length=60)
    zip_code = models.CharField(max_length=20, null=True, blank=True)
    country = CountryField(blank_label="(select country)")

    class Meta:
        verbose_name = "Address"
        verbose_name_plural = "Addresses"
        ordering = ['-created']

    def __str__(self):
        return "{} {} {} {}".format(self.house, self.street, self.city, self.country)


class CustomerAddress(models.Model):
    customer = models.ForeignKey(Customer, related_name='addresses', on_delete=models.CASCADE)
    address = models.OneToOneField(Address, related_name='customer', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Customer Address"
        verbose_name_plural = "Customer Addresses"

    def __str__(self):
        return "Address for {} {}".format(self.customer.first_name, self.customer.last_name)
