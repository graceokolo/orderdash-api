from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, DestroyModelMixin, \
    UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from general.models import CustomerPhoneNumber, CustomerAddress, Address
from general.serializers import CustomerPhoneNumberSerializer, CustomerAddressSerializer, AddressSerializer


class CustomerPhoneNumberViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    DestroyModelMixin,
    GenericViewSet,
):
    serializer_class = CustomerPhoneNumberSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        customer_id = self.kwargs['customer_id']
        return CustomerPhoneNumber.objects.filter(customer__pk=customer_id)

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)


class CustomerAddressViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
    GenericViewSet,
):
    serializer_class = CustomerAddressSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        customer_id = self.kwargs['customer_id']
        return CustomerAddress.objects.filter(customer__pk=customer_id)

    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)


class AddressViewSet(
    ListModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    GenericViewSet,
):
    serializer_class = AddressSerializer
    permission_classes = [IsAuthenticated]
    queryset = Address.objects.all()

    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)
