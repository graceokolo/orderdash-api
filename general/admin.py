from django.contrib import admin
from general.models import PhoneNumber, CustomerPhoneNumber, CustomerAddress, Address

admin.site.register(PhoneNumber)
admin.site.register(CustomerPhoneNumber)
admin.site.register(CustomerAddress)
admin.site.register(Address)
